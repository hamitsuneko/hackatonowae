﻿using NeuroSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static System.Net.Mime.MediaTypeNames;

namespace WpfApp4
{
    /// <summary>
    /// Логика взаимодействия для Session.xaml
    /// </summary>
    public partial class Session : Window
    {
        BrainBitSensor bitSensor;
        public double resO1 { get; set; } 
        public double resO2 { get; set; }
        public double resT3 { get; set; }
        public double resT4 { get; set; }

        public Session(BrainBitSensor bitSensor)
        {
            InitializeComponent();
            this.bitSensor = bitSensor;
            CheckResistance();


        }
        void CheckOpen()
        {
            if (resO1 != double.PositiveInfinity && resO2 != double.PositiveInfinity && resT3 != double.PositiveInfinity &&
    resT4 != double.PositiveInfinity)
            {
                var page = new Survey(bitSensor);
                page.Show();
                this.Close();
            }
        }
        void onBrainBitResistDataRecived(ISensor sensor, BrainBitResistData data)
        {
            resO1 = (data.O1 / 1000);
            resO2 = data.O2 / 1000;
            resT3 = data.T3 / 1000;
            resT4 = data.T4 / 1000;

            Dispatcher.Invoke(() =>
            {
                resistValueO1.Text = (resO1 != double.PositiveInfinity).ToString();
                resistValueO2.Text = (resO2 != double.PositiveInfinity).ToString();
                resistValueT3.Text = (resT3 != double.PositiveInfinity).ToString();
                resistValueT4.Text = (resT4 != double.PositiveInfinity).ToString();
              //  resistValue.Text = "O1" + resO1 + "O2:" + resO2 + "T3:" + resT3 + "T4:" +
            //+resT4* 1e3;
                
                /*if (resO1 != double.PositiveInfinity && resO2 != double.PositiveInfinity && resT3 != double.PositiveInfinity &&
    resT4 != double.PositiveInfinity)
                {
                    var page = new Survey(bitSensor);
                    page.Show();
                    this.Close();
                    
                    return;
                }*/

            });
            Console.WriteLine($"O1: {data.O1 * 1e3} O2: {data.O2 * 1e3} T3: {data.T3 * 1e3} T4: {data.T4 * 1e3}");

        }

        private async void CheckResistance()
        {
            bitSensor.EventBrainBitResistDataRecived += onBrainBitResistDataRecived;
            // proverka rezistansov
            bitSensor.ExecCommand(SensorCommand.CommandStartResist);
            //await Task.Delay(15000);
            

        }
        // проверка сопротивления на электродах
        private void Button_Click_Resist(object sender, RoutedEventArgs e)
        {
            if (resO1 != double.PositiveInfinity && resO2 != double.PositiveInfinity && resT3 != double.PositiveInfinity &&
            resT4 != double.PositiveInfinity)
            {
                bitSensor.ExecCommand(SensorCommand.CommandStopResist);
                bitSensor.EventBrainBitResistDataRecived -= onBrainBitResistDataRecived;
                var page = new Survey(bitSensor);
                page.Show();
                this.Close();

            }
        }

    }
}
