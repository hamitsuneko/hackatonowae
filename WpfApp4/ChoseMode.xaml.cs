﻿using NeuroSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp4
{
    /// <summary>
    /// Логика взаимодействия для ChoseMode.xaml
    /// </summary>
    public partial class ChoseMode : Window
    {
        public BrainBitSensor bitSensor;
        int sliderRelValue;
        int sliderAtValue;

        public ChoseMode(BrainBitSensor bitSensor, int rel, int at)
        {
            InitializeComponent();
            this.bitSensor = bitSensor;
            this.sliderAtValue = at;
            this.sliderRelValue = rel;
        }

        // Здесь запускаем калибровку расслабления
        private void Button_Click_Att(object sender, RoutedEventArgs e)
        {
          var page = new CalibrRel(bitSensor, sliderAtValue, sliderRelValue);
            page.Show();    
            this.Close();
        }

        // Здесь запускаем калибровку концентрации
        private void Button_Click_Rel(object sender, RoutedEventArgs e)
        {
            var page = new CalibrAtt(bitSensor, sliderAtValue, sliderRelValue);
            page.Show();
            this.Close();
        }
    }
}
