﻿using NeuroSDK;
using SignalMath;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp4
{
    /// <summary>
    /// Логика взаимодействия для SessionInProgress.xaml
    /// </summary>
    public partial class SessionInProgress : Window
    {
        public BrainBitSensor bitSensor;
        int sliderRelValue;
        int sliderAtValue;


        EegEmotionalMath math;

        public SessionInProgress(BrainBitSensor bitSensor, int rel, int at, EegEmotionalMath math)
        {
            InitializeComponent();
            this.math= math;
            this.bitSensor = bitSensor;
            this.sliderAtValue = at;
            this.sliderRelValue = rel;

            
        }
        public void GetMentalData()
        {
            MindData[] mentalData = math.ReadMentalDataArr();

            foreach (var m in mentalData)
            {
                

            }
        }
        
    }
}
