﻿using NeuroSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp4
{
    /// <summary>
    /// Логика взаимодействия для MainPage.xaml
    /// </summary>
    public partial class MainPage : Window
    {
        private BrainBitSensor bitSensor;
        public MainPage(BrainBitSensor bitSensor)
        {
            InitializeComponent();
            this.bitSensor = bitSensor;

           // bitSensor.EventSensorStateChanged += BitSensor_EventSensorStateChanged;
           // bitSensor.EventBatteryChanged += BitSensor_EventBatteryChanged;

            TextAdress.Text = bitSensor.Address;
            textState.Text = bitSensor.State == SensorState.StateInRange? "Connected" : "Disconnected";
            textBattery.Text = bitSensor.BattPower.ToString() + "%";


        }

        private void BitSensor_EventBatteryChanged(ISensor sensor, int battPower)
        {
            textBattery.Text = bitSensor.BattPower.ToString() + "%";
        }

        private void BitSensor_EventSensorStateChanged(ISensor sensor, SensorState sensorState)
        {
            textState.Text = bitSensor.State == SensorState.StateInRange ? "Connected" : "Disconnected";
        }

        private void Button_Click_Start(object sender, RoutedEventArgs e)
        {
            // открываем страничку с текущей сессией
            var page = new Session(bitSensor);
            page.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var setting = new Settings();
            setting.Show();
        }

       
    }
}
