﻿using NeuroSDK;
using SignalMath;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfApp4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        private string string1 = "Прежде чем продолжить, необходимо ввести адрес устройства";
        private string string2 = "Выполняется подключение к устройству\r\nЭто может занять некоторое время...";
        private string string3 = "Подключиться к устройству не удалось\r\nПопробуйте ввести адрес еще раз";

        private byte mode = 0;

        private BrainBitSensor? sensorBit;
        private Scanner scanner;
        public MainWindow()
        {
            InitializeComponent();
            try
            {


                scanner = new Scanner(SensorFamily.SensorLEBrainBit);
            }
            catch (Exception ex){
                Console.WriteLine(ex.ToString());
                    }

        }

       

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button? button = sender as Button;
            if (button == null) return;

            switch (mode)
            {
                case 0:
                    {
                        if (adress.Text  == "") return;
                        adress.Visibility = Visibility.Hidden;
                        label.Margin = new Thickness(0, 0, 0, 40);
                        label.Content = string2;

                        
                        button.Content = "Назад";
                        mode = 1;
                        scanner.Start();
                        waiting();
                    }
                    break;
                case 1:
                    {
                        scanner.Stop();//конец поиска

                        adress.Visibility = Visibility.Visible;
                        button.Content = "Подключить";
                        label.Margin = new Thickness(0, 0, 0, 120);
                        label.Content = string1;
                        mode = 0;
                    }
                    break;
                case 2:
                    {
                        adress.Visibility = Visibility.Visible;
                        button.Content = "Подключить";
                        label.Margin = new Thickness(0, 0, 0, 120);
                        label.Content = string1;
                        mode = 0;
                    }
                    break;

            }


        }


        private async void waiting()
        {
            await Task.Delay(5000);
            scanner.Stop();
            var list = scanner.Sensors;
            
            foreach(var item in list)
            {
                if (item.Address == adress.Text)
                {
                    sensorBit = scanner.CreateSensor(item) as BrainBitSensor;
                    if (sensorBit != null)
                    {
                        // Устройство подключено - открываем главную
                        var page = new MainPage(sensorBit);
                        page.Show();
                        this.Close();

                    }
                }
            }
            
            label.Content = string3;
            mode = 2;
        }

        ~MainWindow()
        {
            scanner.Dispose();//конец работы с устройством
        }

        private void adress_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
