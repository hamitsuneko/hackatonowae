﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp4
{
    public class SessionData
    {
        //дата
        public string date { get; set; }
        //время сессии
        public  int time { get; set; } 
        //массивы данных релаксации/концентрации
        public double[] data { get; set; }


    }
}
