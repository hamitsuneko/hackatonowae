﻿using NeuroSDK;
using SignalMath;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp4
{
    /// <summary>
    /// Логика взаимодействия для CalibrRel.xaml
    /// </summary>
    public partial class CalibrRel : Window
    { 
        
        SoundPlayer soundPlayer = new SoundPlayer(soundLocation: @"C:\Users\potap\source\repos\WpfApp5\WpfApp4\WpfApp4\Witcher.wav");
        
        public BrainBitSensor bitSensor;
        int sliderRelValue;
        int sliderAtValue;

        public EegEmotionalMath math;

         List <double> finalData= new List <double> (1);

        int k = 0;

        double[] finalDataArr;

        public CalibrRel(BrainBitSensor bitSensor, int rel, int at)
        {
            InitializeComponent();
            this.bitSensor = bitSensor;
            this.sliderAtValue = at;
            this.sliderRelValue = rel;



            Console.WriteLine("knopka");


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            uint samplingFrequency = 250;
            var mls = new MathLibSetting
            {
                sampling_rate = samplingFrequency,
                process_win_freq = 25,
                n_first_sec_skipped = 4,
                fft_window = samplingFrequency * 2,
                bipolar_mode = true,
                channels_number = 4,
                channel_for_analysis = 0
            };
            var ads = new ArtifactDetectSetting
            {
                art_bord = 110,
                allowed_percent_artpoints = 70,
                raw_betap_limit = 800_000,
                total_pow_border = 400_000_000,
                global_artwin_sec = 4,
                spect_art_by_totalp = false,
                num_wins_for_quality_avg = 125,
                hanning_win_spectrum = true,
                hamming_win_spectrum = false
            };
            var sads = new ShortArtifactDetectSetting
            {
                ampl_art_detect_win_size = 200,
                ampl_art_zerod_area = 200,
                ampl_art_extremum_border = 25
            };

            var mss = new MentalAndSpectralSetting
            {
                n_sec_for_averaging = 2,
                n_sec_for_instant_estimation = 4
            };

            math = new EegEmotionalMath(mls, ads, sads, mss);

            // type of evaluation of instant mental levels
            bool independentMentalLevels = false;
            math.SetMentalEstimationMode(independentMentalLevels);

            // number of windows after the artifact with the previous actual value
            int nwinsSkipAfterArtifact = 10;
            math.SetSkipWinsAfterArtifact(nwinsSkipAfterArtifact);

            // calculation of mental levels relative to calibration values
            math.SetZeroSpectWaves(true, 0, 1, 1, 1, 0);

            // spectrum normalization by bandwidth
            math.SetSpectNormalizationByBandsWidth(true);

            this.math.StartCalibration();

            bitSensor.EventBrainBitSignalDataRecived += onBrainBitSignalDataRecived;
            Thread.Sleep(11000);


            buttonReady.Visibility = Visibility.Visible;


            SessionInProgress page = new SessionInProgress(bitSensor, sliderRelValue, sliderAtValue, math);


            bitSensor.ExecCommand(SensorCommand.CommandStartSignal);       

        }


        private async void start()
        {
            bitSensor.ExecCommand(SensorCommand.CommandStartSignal);
            bitSensor.EventBrainBitSignalDataRecived += onBrainBitSignalDataRecived;
            Thread.Sleep(11000);

           

            SessionInProgress page = new SessionInProgress(bitSensor, sliderRelValue, sliderAtValue, math);
            //page.Show();
            // this.Close();
        }

        private bool flag = true;

        private void onBrainBitSignalDataRecived(ISensor sensor, BrainBitSignalData[] data)
        {
            // начать калибровку
            var samples = new RawChannels[data.Count()];

            for (int i = 0; i < samples.Count(); i++)
            {

                samples[i].LeftBipolar = data[i].T3 - data[i].O1;
                samples[i].RightBipolar = data[i].T4 - data[i].O2;

            }

            math.PushData(samples);
            math.ProcessDataArr();//

            if (math.IsBothSidesArtifacted())
                Console.WriteLine("Наденьте на голову BrainBit");
            else

            if (!math.CalibrationFinished())
            {

                Console.WriteLine(math.GetCallibrationPercents());
            }
            else
            {
                if(flag)
                {
                    soundPlayer.Play();
                    flag = false;
                }
                // Reading mental levels in percent
                MindData[] mentalData = math.ReadMentalDataArr();

                // Reading relative spectral values in percent
                SpectralDataPercents[] spData = math.ReadSpectralDataPercentsArr();


                foreach (var m in mentalData)
                {
                    Console.WriteLine("Концентрация - " + m.RelAttention);
                    Console.WriteLine("Релаксация - " + m.RelRelaxation);
                    Dispatcher.Invoke(() =>
                    {

                        label_Copy.Visibility = Visibility.Hidden;
                        label_Copy1.Visibility = Visibility.Hidden;
                        label_Copy2.Visibility = Visibility.Hidden;
                        circle.Visibility = Visibility.Hidden;
                        buttonReady.Visibility = Visibility.Hidden;

                        SessionEnd.Visibility = Visibility.Visible;

                        Attention.Visibility = Visibility.Visible;
                        SessionEnd.Visibility = Visibility.Visible;
                        Attention.Text = "Уровень относительной концентрации: " + m.RelAttention.ToString();

                    });
                    k++;
                    if (k == 15)
                    {
                        k = 0;
                        finalData.Add(m.RelAttention);
                    }

                }

            }
        }

        private void Button_Click_End_Session(object sender, RoutedEventArgs e)
        {
            
            SessionEnd.Visibility = Visibility.Hidden;
            finalData[0] = 0;
            bitSensor.ExecCommand(SensorCommand.CommandStopSignal);
            bitSensor.EventBrainBitSignalDataRecived -= onBrainBitSignalDataRecived;

            finalDataArr = finalData.ToArray();
            label_Copy1.Visibility = Visibility.Hidden;
            label_Copy2.Visibility = Visibility.Hidden;
            label.Visibility = Visibility.Hidden;
            buttonReady.Visibility = Visibility.Hidden;
            circle.Visibility = Visibility.Hidden;

            RelAtChart.Visibility = Visibility.Visible;
            RelAtChart.Plot.AddSignal(finalDataArr);
            RelAtChart.Plot.YAxis.SetBoundary(-20, 100);
            //RelAtChart.Plot.YAxis.SetZoomInLimit(1000);
            RelAtChart.Plot.Title("График концентрации");
            RelAtChart.Plot.SaveFig("Axis_label.png");
            RelAtChart.Refresh();
        }
    }
}
