﻿using NeuroSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp4
{
    /// <summary>
    /// Логика взаимодействия для Survey.xaml
    /// </summary>
    public partial class Survey : Window
    {
        public BrainBitSensor bitSensor;
        int sliderRelValue = 1;
        int sliderAtValue = 1;

        public Survey(BrainBitSensor bitSensor)
        {
            InitializeComponent();
            this.bitSensor = bitSensor;
        }

        // Значение от 1 до 10 концентрация
        private void Slider_Att_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.sliderAtValue = (int)SliderAttention.Value + 1;
            Console.WriteLine(this.sliderAtValue);
        }
        // Значение от 1 до 10 расслабление
        private void Slider_Rel_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.sliderRelValue = (int)SliderRelaxation.Value + 1;
            Console.WriteLine(this.sliderRelValue.ToString());
        }

        private void Button_Click_Start(object sender, RoutedEventArgs e)
        {
            var page = new ChoseMode(bitSensor, sliderRelValue, sliderAtValue);
            page.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var page = new MainPage(this.bitSensor);
            page.Show();
            this.Close();
        }
    }
}
